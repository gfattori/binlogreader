# binLogReader


TCS log file parser

## Example use

See 'binlogreader.ipynb'

## License
MIT license 

## Project status

Parsing:
* int32 and int32 array
* uint32 and uint32 array
* float32 and float32 array
* double64 and double64 array
* string and string array
* string-string map




